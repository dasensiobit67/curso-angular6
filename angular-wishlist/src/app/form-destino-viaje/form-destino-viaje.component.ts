import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DestinoViaje } from '../models/destino-viaje.model';


@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg:FormGroup;

  constructor(private fb:FormBuilder) {
    this.onItemAdded=new EventEmitter();
    this.fg=this.fb.group({
      nombre:['',Validators.required],
      url:['',Validators.required]
    });

    //observador de tipeo
    this.fg.valueChanges.subscribe((form:any)=>{
      console.log('cambio en el form:',form);
    })
   }

  ngOnInit(): void {
  }

  guardar(nombre:string, url:string):boolean{
    const d = new DestinoViaje(nombre,url);
    this.onItemAdded.emit(d);
    return false;
  }

}
